# Backend docker image

## How to run:
Execute command `docker-compose up` with [docker-compose-file](https://gitlab.com/larry-brand-devops-aws-infra-example/ci-cd/kubernetes/-/blob/master/local/docker-compose.yml)

or execute command

`$docker run --name example-app-bff -itd -p 8081:8080 larrybrand/devops-aws-infra-example-bff`

## How to run in k8s cluster:
Open port 8080 and pass environment variables "backend.url=http://example-app-backend.default.svc.cluster.local:8080"
