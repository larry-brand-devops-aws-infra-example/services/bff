package com.hello.application.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
@Configuration
@CrossOrigin(origins = {"*"})
@Slf4j
public class HelloController {

    @Value("${backend.url}")
    private String backendUrl;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity getHelloMessage() {
        return ResponseEntity.ok()
                             .body("Hello from bff!!!");
    }

    @RequestMapping(value = "/getOrders", method = RequestMethod.GET)
    public ResponseEntity getOrders() {
        RestTemplate restTemplate = new RestTemplate();
        log.info("calling " + backendUrl + "/backend-api/getOrders");
        String result = restTemplate.getForObject(backendUrl + "/backend-api/getOrders", String.class);
        log.info("getOrders from bff: " + result);
        return ResponseEntity.ok()
                .body("getOrders from bff: " + result);
    }
}
